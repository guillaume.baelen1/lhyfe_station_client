from lhyfe_station_client.api import Api
from lhyfe_station_client.validation import Validation
from lhyfe_station_client.credentials import Credentials

def save_credentials(   
    api_url: str,
    api_key: str,
    api_secret: str,
    client_id: str,
    client_secret: str
    ):
    
    credentials: Credentials = Credentials()     
    
    credentials.set_api_url(api_url)
    credentials.set_api_key(api_key)
    credentials.set_api_secret(api_secret)
    credentials.set_client_id(client_id)
    credentials.set_client_secret(client_secret)
    
def send_data(site_name: str, sensor_name: str, data: dict):
    validation: Validation = Validation()
    api: Api = Api()
    
    if not validation.check_keys(data):
        raise Exception("Missing keys in your data payload.")
    
    if not validation.check_values_type(data):
        raise Exception("Wrong value type in data")
    
    site_id = api.get_site_id(site_name)
    sensor_id = api.get_sensor_id(site_id, sensor_name)
        
    if "sensor_id" not in data:
        data["sensor_id"] = sensor_id
        
    api.upload_sensor_value(sensor_id, data=data)