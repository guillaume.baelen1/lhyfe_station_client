class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Credentials(metaclass=Singleton):
    def __init__(self, 
                 api_url: str = None, 
                 api_key: str = None, 
                 api_secret: str = None, 
                 client_id: str = None, 
                 client_secret: str = None):
        
        self.api_url: str = api_url
        self.api_key: str = api_key
        self.api_secret: str = api_secret
        self.client_id: str = client_id
        self.client_secret: str = client_secret
        
    def set_api_url(self, api_url: str):
        self.api_url = api_url
        
    def set_api_key(self, api_key: str):
        self.api_key = api_key
        
    def set_api_secret(self, api_secret: str):
        self.api_secret = api_secret
        
    def set_client_id(self, client_id: str):
        self.client_id = client_id
        
    def set_client_secret(self, client_secret: str):
        self.client_secret = client_secret
        
    def get_api_url(self):
        return self.api_url
        
    def get_api_key(self):
        return self.api_key
        
    def get_api_secret(self):
        return self.api_secret
        
    def get_client_id(self):
        return self.client_id
        
    def get_client_secret(self):
        return self.client_secret