import base64
import json
import boto3
import hmac
import hashlib
import jwt
import requests

from lhyfe_station_client.credentials import Credentials

class ApiConnector:
    """
    This class will handle the connection with the Lhyfe Api
    """
    def __init__(self) -> None:
        self.client: any = boto3.client('cognito-idp', 'eu-central-1')
        self.credentials: Credentials = Credentials()
        
    def __generate_hash(self, api_key, client_id, client_secret) -> str:
        Signature: hmac.HMAC = hmac.new(client_secret, api_key+client_id, digestmod=hashlib.sha256)
        Hash: bytes = base64.b64encode(Signature.digest())
        
        return Hash.decode('utf-8')
    
    def __get_token(self) -> str:
        response = self.client.initiate_auth(
            ClientId=self.credentials.get_client_id(),
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': self.credentials.get_api_key(),
                'PASSWORD': self.credentials.get_api_secret(),
                'SECRET_HASH': self.__generate_hash(self.credentials.get_api_key().encode('utf-8'), self.credentials.get_client_id().encode('utf-8'), self.credentials.get_client_secret().encode('utf-8'))
            }
        )
        
        return response['AuthenticationResult']['IdToken']
    
    def __add_pagination_params(self, limit: int, offset: int, filters: str) -> str:
        """
        Function to format parameters for pagination to add to get request when needed.

        :param limit: maximum number of elements to returned by the api.
        :param offset: row number in the list of element to start fetching from.
        :param filters: list of column with value to filter the result of the api by values. Each filter must be seperated by a '|'.

        :returns: a formated string with the parameters. For example: limit=1&offset=0&filters="name=test|type=consumption"
        """
        params = []

        if limit is not None:
            params.append(f"limit={limit}")

        if filters is not None:
            params.append(f"filters={filters}")

        if offset is not None:
            params.append(f"offset={offset}")

        return "&".join(params)
    
    def make_request(self, method: str, path: str, data: dict = None, limit: int = None, offset: int = None, filters: str = None, retry=0) -> str:
        """
        Generic function to encapsulate a http request from the params, adding with the proper headers and making the request.

        :param method: this is the http method to use to make the request, to be chosen from: "GET", "POST", "PATCH", "PUT", "DELETE"
        :param path: this is a string of the path coming after the version in the request url.
        :param data: this is a dictionnary with the data to pass to the body
        :param limit: maximum number of elements to returned by the api.
        :param offset: row number in the list of element to start fetching from.
        :param filters: list of column with value to filter the result of the api by values. Each filter must be seperated by a '|'.
        :param retry: to retry the request, defaults to 0, raise after 2 more tryouts
        :returns: the response of the api as a json
        :raises Exception: raises an exception when the request fails
        """
        headers = {
            "Authorization": f"Bearer {self.__get_token()}",
        }

        if method == "GET":
            response = requests.get(
                f"{self.credentials.get_api_url()}/{path}?{self.__add_pagination_params(limit, offset, filters)}",
                headers=headers,
                timeout=10,
            )
        elif method == "POST":
            data = json.loads(json.dumps(data))
            response = requests.post(f"{self.credentials.get_api_url()}/{path}", json=data, headers=headers, timeout=10)
        else:
            raise ValueError(f"{method} is not recognize please use one among GET, HEAD, POST, PATCH, PUT")

        if response.ok:
            if response.status_code == 204:
                return response.headers, ""

            try:
                return response.headers, response.json()
            except requests.JSONDecodeError:
                # retry twice before returning
                if retry < 2:
                    retry += 1
                    # in case the token is expired
                    self.access_token = self.get_token()["access_token"]
                    return self.make_request(
                        method, path, data, limit=limit, offset=offset, filters=filters, retry=retry
                    )
                else:
                    raise Exception(
                        f"Request to lhyfe api returns a JSONDecodeError, even after two retries, headers: {response.headers}\n content: {response.content};\n status_code: {response.status_code}; reason: {response.reason}."
                    )
        elif response.status_code == 400:
            try:
                jwt.decode(self.access_token, options={"verify_signature": False})
            except jwt.ExpiredSignatureError:
                self.access_token = self.get_token()["access_token"]
                return self.make_request(method, path, data, limit=limit, offset=offset, filters=filters)
            raise ValueError(response.json()["message"])
        else:
            raise Exception(
                f"Unable to make request to lhyfe api, headers: {response.headers}\nn content: {response.content};\n status_code: {response.status_code}; reason: {response.reason}."
            )

class Api:
    """
    This class will handle the calls to the Api
    """
    def __init__(self) -> None:
        self.conn: ApiConnector = ApiConnector()
    
    def get_site_id(self, site_name: str) -> list[dict]:
        _, body = self.conn.make_request("GET", "sites", limit=200, filters=f"name={site_name}")
        return body[0]['id']
    
    def get_sensor_id(self, site_id: int, sensor_name: str) -> list[dict]:
        _, body = self.conn.make_request("GET", f"sites/{site_id}/sensors", limit=200, filters=f"alias={sensor_name}")
        return body[0]['id']
    
    def upload_sensor_value(self, sensor_id: int, data: dict):
        _, body = self.conn.make_request("POST", f"sensors/{sensor_id}/values", data=data)
        return body