class Validation:
    """
    This class will handle the validation of the data to be sent.
    """
    def check_keys(self, data: dict) -> bool:
        required_keys = [
            "pressure",
            "pressure_sensor_temperature",
            "timestamp"
        ]
        
        if all(key in required_keys for key in data):
            return True
        
        return False
    
    def check_values_type(self, data: dict) -> bool:
        if type(data["pressure"]) is not float:
            return False
        
        if type(data["pressure_sensor_temperature"]) is not float:
            return False
        
        if type(data["timestamp"]) is not str:
            return False
        
        return True 