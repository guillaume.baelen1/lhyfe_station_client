
# To send data to Lhyfe’s api, all you need are:
#           * the name of the site,
#           * the name of the sensor that is installed on the station
#           * the data collected from this sensor
# 
# Then all you need to do is to call is this:
#
#   lhyfe_station_client.send_data(site_name, sensor_name, data)


###################################################################
#                                                                 #
#                            Example                              #
#                                                                 #
###################################################################
#
# The code below is an example on how to use our api to send data
#

import random # this is only for the example


# Step 1: import the required librairie
import lhyfe_station_client
from datetime import datetime

# Step 2: save your api credentials

lhyfe_station_client.save_credentials(
    api_url = <url_of_the_api>,
    api_key = <api_key>,
    api_secret = <api_secret>,
    client_id= <client_id>,
    client_secret = <client_secret>
)

# Step 3: generate or get the station pressure and send them using the function send_data of lhyfe_station_client.
# In this code, we will generate random data to mock a station

site_name = <site_name>
sensor_name = <sensor_name>

for i in range(0, 10000):
    pressure: float = random.uniform(50, 350)
    pressure_sensor_temperature: float = random.uniform(-15, 40)
    timestamp: str = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ") # This timestamp needs to be UTC to the format YYYY-MM-DDTHH:mm:ss.msZ
    
    data = {
        "pressure": pressure,
        "pressure_sensor_temperature": pressure_sensor_temperature,
        "timestamp": timestamp
    }

    lhyfe_station_client.send_data(site_name, sensor_name, data)
    print(f"data: pressure={pressure}, pressure_sensor_temperature={pressure_sensor_temperature}, timestamp={timestamp} sent")